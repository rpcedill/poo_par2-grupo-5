/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menuNegocio;

/**
 *
 * @author Rainiero
 */
public class Atencion {     //atributos de la clase atencion
    private Cita cita;
    private Servicio servicio;
    private Empleado empleado;
    private Cliente cliente;
    private int duracionReal;   //constructores
                
    public Atencion(Cita cita, Servicio servicio, Empleado empleado, Cliente cliente, int duracionReal) {
        this.cita = cita;
        this.servicio = servicio;
        this.empleado = empleado;
        this.cliente = cliente;
        this.duracionReal = duracionReal;
    }
    
   //getters y setters
    public int getDuracionReal() {
        return duracionReal;
    }

    public void setDuracionReal(int duracionReal) {
        this.duracionReal = duracionReal;
    }
    public void citaAtendida(){
        cliente.getCitas().remove(cita);
    }
    public Cita getCita(){
        return cita;
    }
    
       //metodo to String
    @Override
    public String toString(){
        return "Atencion: {Servicio: "+servicio+", Duracion: "+duracionReal+ "Ciente: " + cliente.getNombre() + "Empleado: "+ empleado.getNombre()+"}";

    }
}