/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menuNegocio;

/**
 *
 * @author Gustavo
 */
public abstract class  Persona {
    private String cedula;
    private String nombre;
    private String telefono;
    private String email;
    
    Persona(String cd){
        cedula = cd;
    }
    Persona (String cd, String n,String t, String e){
    cedula = cd;
    nombre = n;
    telefono = t;
    email = e;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    @Override
    public String toString(){
        return "Cedula: "+cedula+", Nombre: "+nombre+", Telefono: "+telefono+", Email: "+email;
    }
    
    @Override
    public boolean equals(Object obj){
        if(this == obj){
        return true;
        }
        if(obj!=null && obj instanceof Persona){
            Persona other = (Persona)obj;
            return cedula.equals(other.cedula);
        }
        return false;
    }
}