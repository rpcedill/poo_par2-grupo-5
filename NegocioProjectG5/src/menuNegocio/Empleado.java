/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menuNegocio;

/**
 *
 * @author Rainiero
 */
public class Empleado extends Persona {   //atributos de la clase empleado que es hija de persona
  private boolean estado;
  public Empleado(String cedula){
    super(cedula);
  }
    
  public Empleado(String cedula, String nombre,String telefono, String email, boolean estad){   //constructores
    super(cedula,nombre, telefono, email);
    estado = estad;
  }
   //setters and getters

  public boolean getEstado() {
    return estado;
  }

  public void setEstado(boolean estado) {
    this.estado = estado;
  }
  
           //metodo toString

  @Override
  public String toString(){
    return "Empleado: {"+super.toString()+"EstadoActivo: "+ estado +"}";
  }
  
         //metodo equals

  public boolean equals(Object obj){
        if(this==obj){
            return true;
        }
        if(obj!=null && getClass() == obj.getClass()){
            Empleado employeer = (Empleado)obj;
            return getCedula().equals(employeer.getCedula());
        }
        return false;
    }
}
