/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menuNegocio;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 */
public class Cliente extends Persona{ //atributos de la clase cliente que es hija de persona
  private ArrayList<Cita> citas;

  public Cliente(String cedula){
    super(cedula);
    citas = new ArrayList<>();
  }
  public Cliente (String cedula, String nombre, String telefono, String email, ArrayList<Cita> c){    //constructores
    super(cedula,nombre,telefono,email);
    citas = c;
  }
  public Cliente (String cedula, String nombre, String telefono, String email){
    super(cedula,nombre,telefono,email);
    citas = new ArrayList<>();
  }
   //setters and getters

    public ArrayList<Cita> getCitas() {
        return citas;
    }
  
    
               //metodo toString

    
  @Override
  public String toString(){
    return "Cliente: {"+super.toString() + "}";
  }

               //metodo equals

  
  public boolean equals(Object obj){
        if(this == obj){
        return true;
        }
        if(obj!=null && getClass()==obj.getClass()){
            Cliente other = (Cliente)obj;
            return getCedula().equals(other.getCedula());
        }
        return false;
    }
     
}