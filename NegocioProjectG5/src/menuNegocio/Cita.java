/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menuNegocio;


/**
 *
 * @author Rainiero
 */
public class Cita {   //atrivutos clase cita
  private String fecha;
  private String hora;
  private Cliente cliente;
  private Empleado empleado;
  
  public Cita(String f){   //constructores
        fecha =f;
    }
  
  public Cita(String f,String h, Cliente c, Empleado e){
    fecha=f;
    hora=h;
    cliente = c;
    empleado = e;
  }
   //setters and getters
  public String getFecha() {
    return fecha;
  }

  public void setFecha(String fecha) {
    this.fecha = fecha;
  }

  public String getHora() {
    return hora;
  }

  public void setHora(String hora) {
    this.hora = hora;
  }
       //metodo toString
  @Override
  public String toString(){
    return "Cita: {Fecha: "+fecha+", Hora: "+hora+ "Ciente: " + cliente.getNombre() + "Encargado: "+ empleado.getNombre()+"}";
  }
    
}
