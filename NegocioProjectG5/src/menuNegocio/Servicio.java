/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menuNegocio;

/**
 *
 * @author Rainiero
 */
public class Servicio { //atributos de la clase servicio
  private String nombreS;
  private int duracion;
  private double precio;
  private boolean estadoServicio;

  public Servicio(String nombreS) {     //constructores
    this.nombreS = nombreS;
  }
    
    
  public Servicio(String nombre, int d, double pr, boolean estServ){
    nombreS = nombre;
    duracion = d;
    precio = pr;
    estadoServicio = estServ;
  }
   //setters and getters

  public String getNombreS() {
    return nombreS;
  }

  public void setNombreS(String nombreS) {
    this.nombreS = nombreS;
  }

  public int getDuracion() {
    return duracion;
  }

  public void setDuracion(int duracion) {
    this.duracion = duracion;
  }

  public double getPrecio() {
    return precio;
  }

  public void setPrecio(double precio) {
    this.precio = precio;
  }

  public boolean getEstadoServicio() {
    return estadoServicio;
  }

  public void setEstadoServicio(boolean estadoServicio) {
    this.estadoServicio = estadoServicio;
  }
    
           //metodo toString

  @Override
  public String toString(){
    return "Servicio { Nombre Servicio: "+ nombreS+ ", Duracion: "+duracion+", Precio: "+precio+", Estado del servicio:"+estadoServicio+ "}";
  }
}
