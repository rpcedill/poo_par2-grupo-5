/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemaAtencion;
import java.util.ArrayList;
import java.util.Scanner;
import menuNegocio.*;

/**
 *
 * @author Rainiero
 */
public class Negocio {                    //atributos clase negocio
    private String nombreN;
    private String emailN;
    private String telefonoN;
    private String direccion;
    private Empleado administrador;
    private ArrayList<Cita> citas;
    private ArrayList<Persona> personas;
    private ArrayList<Atencion> atenciones;
    private ArrayList<Servicio> servicios;
    private String horaFinAtencion;
    private String horaInicioAtencion;
    
    public Negocio(){           //constructor por defecto
        administrador = new Empleado("987654321", "Emilio Andrade".toUpperCase(), "087654321", "emilio@gmail.com".toUpperCase(),true);
        nombreN= "Servicios Especiales S.A.".toUpperCase();
        direccion = "Av. Francisco de Orellana, Guayaquil".toUpperCase();
        emailN = "serves@gmail.com".toUpperCase();
        telefonoN = "02154898784";
        horaInicioAtencion = "07:00";
        horaFinAtencion = "21:00";
    }
    //cargar datos previos al sistema
    public void inicializarSistema(){
        personas = new ArrayList<>();
        citas = new ArrayList<>();
        servicios = new ArrayList<>();
        atenciones = new ArrayList<>();
        personas.add(administrador);
        crearEmpleados();
        crearClientes();
        crearServicios();
        crearCitas();
        crearAtenciones();

    }
    //MAIN
    public static void main(String[] args) {
        Negocio empresa = new Negocio();
        empresa.inicializarSistema();
        empresa.mostrarMenu();
        
    }
    
    //crear clientes, empleados, atenciones,citas,servicios
    public void crearClientes(){
        personas.add(new Cliente("123456789", "JUAN NUÑEZ", "0123456789", "JUAN@GMAIL.COM"));
        personas.add(new Cliente("123456788", "SUSANA TORRES", "0123456788", "SUSANA@GMAIL.COM"));
    }
    public void crearEmpleados(){
        personas.add(new Empleado("887465123", "LUCAS SOLIS", "087465123", "LUCAS@GMAIL.COM",true));
        personas.add(new Empleado("125487987", "MARIA PEREZ", "0125487987", "MARIA@GMAIL.COM",true));
    }
    public void crearCitas(){
        citas.add(new Cita("15/02/2020", "18:00", (Cliente)personas.get(4), (Empleado)personas.get(2)));
        Cliente c01 = (Cliente)personas.get(4);
        c01.getCitas().add(citas.get(0));
        citas.add(new Cita("16/05/2020", "19:00",(Cliente)personas.get(4), (Empleado)personas.get(1)));
        c01.getCitas().add(citas.get(1));
        citas.add(new Cita("16/07/2020", "05:00", (Cliente)personas.get(3), (Empleado)personas.get(1)));
        Cliente c02 = (Cliente)personas.get(3);
        c02.getCitas().add(citas.get(2));
        
    }
    public void crearServicios(){
        servicios.add(new Servicio("MASAJE",50,25.00,true));
        servicios.add(new Servicio("LAVADO",40,20.00,true));
    }
    public void crearAtenciones(){
        atenciones.add(new Atencion(citas.get(0), servicios.get(0),(Empleado)personas.get(2), (Cliente)personas.get(3),  70));
        atenciones.get(0).citaAtendida();
    }
    // setters and getters
    
    public String getNombreN() {
        return nombreN;
    }

    public void setNombreN(String nombreN) {
        this.nombreN = nombreN;
    }

    public String getEmailN() {
        return emailN;
    }

    public void setEmailN(String emailN) {
        this.emailN = emailN;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefonoN() {
        return telefonoN;
    }

    public void setTelefonoN(String telefonoN) {
        this.telefonoN = telefonoN;
    }

    public String getHoraInicioAtencion() {
        return horaInicioAtencion;
    }

    public void setHoraInicioAtencion(String horainicioAtencion) {
        this.horaInicioAtencion = horainicioAtencion;
    }

    public String getHoraFinAtencion() {
        return horaFinAtencion;
    }

    public void setHoraFinAtencion(String horafinAtencion) {
        this.horaFinAtencion = horafinAtencion;
    }

    public Empleado getAdministrador() {
        return administrador;
    }

    public void setAdministrador(Empleado administrador) {
        this.administrador = administrador;
    }
    
    // metodo tostring
    @Override
    public String toString(){
        return "Negocio: { Nombre:"+nombreN+"\n Telefono:"+telefonoN+"\n Email:"+emailN+"\n Direccion:"+direccion+"\n "
                + "HoraInicioAtencion:"+horaInicioAtencion+"\n HoraFinAtencion:"+horaFinAtencion+"\n "
                + "Administrador:"+administrador.getNombre()+"}";
    }
    // consultar cita por fecha
    public void consultarCita(String fecha ){
        Cita micita = new Cita(fecha);
        for(Cita citaL : citas){
            if(micita.equals(citaL)){
            System.out.println(citaL);
            }
        }
    }
    public void mostrarMenu(){          //menu del programa
        Scanner sc = new Scanner(System.in);
        int op= 1;
        while ((1<=op) && (op<=7)){
            System.out.println(" 1. Negocio\n 2. Servicios\n 3. Empleados\n 4. Clientes\n 5. Citas\n 6. Atenciones\n 7.Salir\n"
                    + "Ingrese el número de la opción que desea: ");
            int op1 = sc.nextInt();
            sc.nextLine();
            if (op1 ==1){           //opciones para negocio
                System.out.println(this.toString());
                System.out.print("¿Desea modificar datos (S/N)?");
                String modifica= sc.nextLine().toUpperCase();
                if(modifica.equals("S")){
                    this.menuModifica(op1);
                }
            }
            else if (op1 ==2){          //opciones para servicios
                this.mostrarServicios();
                System.out.println("1. Editar servicio\n 2. Agregar Servicio\n 3. Eliminar Servicio\n"
                    + "Ingrese el número de la opción que desea:");
                int opcion = sc.nextInt();
                if(opcion==1){          //editar servicio
                    this.menuModifica(op1);
                }else if (opcion==2){          //agregar servicio
                    sc.nextLine();
                    System.out.println("Ingrese el nombre del Servicio: ");
                    String nombre= sc.nextLine().toUpperCase();
                    sc.nextLine();
                    System.out.println("Ingrese el duracion del Servicio: ");
                    int duracion= sc.nextInt();
                    sc.nextLine();
                    System.out.println("Ingrese el precio del Servicio: ");
                    double precio= sc.nextDouble();
                    sc.nextLine();
                    int d= 0;
                    while (d ==0){
                        d= 1;
                        System.out.println("Ingrese el estado del Servicio(T/F): ");
                        String estado= sc.nextLine().toUpperCase();
                        sc.nextLine();
                        if (estado.equals("T")){
                            servicios.add(new Servicio(nombre,duracion,precio,true));
                        }else if (estado.equals("F")){
                            servicios.add(new Servicio(nombre,duracion,precio,false));
                        }else{
                            System.out.print("Opcion no valida\n");
                            d= 0;
                        }
                    }
                    
                }else if (opcion==3){          //eliminar servicio
                    int h= 0;
                    while (h ==0){
                        this.mostrarServicios();
                        System.out.println("Ingrese el número de la del servicio que desea eliminar: ");
                        int numero = sc.nextInt();
                        sc.nextLine();
                        if ((1<=numero) && (numero<=servicios.size())){
                            servicios.get(numero-1).setEstadoServicio(false);
                            System.out.print("El Servicio : "+servicios.get(numero-1).getNombreS()+" ha sido eliminado con éxito");
                            h=1;
                        }else{
                            System.out.print("Opcion no valida\n");
                        }
                    
                    }
                }else{
                    System.out.print("Opcion no valida\n");
                }
            }
            else if (op1 ==3){          //opciones para empleados
                this.mostrarEmpleados();
                System.out.println("1. Editar Empleado\n 2. Agregar Empleado\n 3. Eliminar Empleado\n"
                    + "Ingrese el número de la opción que desea:");
                int opcion = sc.nextInt();
                if(opcion==1){          //editar empleado
                    this.menuModifica(op1);
                }else if (opcion==2){          //agregar empleado
                    sc.nextLine();
                    System.out.print("ingrese la cedula de la persona que desea ingresar: ");
                    String cedula = sc.nextLine();
                    while(personaExiste(cedula)){  //verifica que persona no este registrada
                        System.out.println("Ya existe persona con esa cedula\nIngrese cedula");
                        cedula = sc.nextLine();  }
                    System.out.println("Ingrese el nombre del Empleado: ");
                    String nombre15= sc.nextLine().toUpperCase();
                    sc.nextLine();                       
                    System.out.println("Ingrese el email del Empleado: ");
                    String email15= sc.nextLine().toUpperCase();
                    sc.nextLine();
                    System.out.println("Ingrese el telefono del Empleado: ");
                    String telefono15= sc.nextLine().toUpperCase();
                    sc.nextLine();
                    int d= 0;
                    while (d ==0){
                        d= 1;
                        System.out.println("Ingrese el estado del Empleado(T/F): ");
                        String estado= sc.nextLine().toUpperCase();
                        sc.nextLine();
                        if (estado.equals("T")){
                            personas.add(new Empleado(cedula,nombre15,telefono15,email15,true));
                        }else if (estado.equals("F")){
                            personas.add(new Empleado(cedula,nombre15,telefono15,email15,false));
                        }else{
                            System.out.print("Opcion no valida\n");
                            d= 0;
                        }
                    }

                }else if (opcion==3){          //eliminar empleado
                    int h= 0;
                    while (h ==0){
                        this.mostrarEmpleados();
                        System.out.println("Ingrese la cedula del empleado que desea eliminar: ");
                        String ced= sc.nextLine().toUpperCase();
                        sc.nextLine();
                        Persona empl = new Empleado(ced);
                        if(personas.contains(empl)){
                            int indice = personas.indexOf(empl);
                            Empleado empl_ = (Empleado)personas.get(indice);
                            empl_.setEstado(false);
                            System.out.print("El empleado con cédula: "+ced+" ha sido eliminado con éxito");
                            h=1;
                        }else{
                            System.out.print("Opcion no valida\n");
                        }
                    }
                    
                }else{
                    System.out.print("Opcion no valida\n");
                }
            }
            else if (op1 ==4){          //opciones para clientes
                this.mostrarClientes();
                System.out.println("1. Editar Cliente\n 2. Agregar Cliente\n"
                    + "Ingrese el número de la opción que desea:");
                int opcion = sc.nextInt();
                if(opcion==1){          //editar cliente
                    this.menuModifica(op1);
                }else if (opcion==2){          //agregar cliente
                    sc.nextLine();
                    System.out.print("ingrese la cedula de la persona que desea ingresar: ");
                    String cedula = sc.nextLine();
                    while(personaExiste(cedula)){  //verifica que persona no este registrada
                        System.out.println("Ya existe persona con esa cedula\nIngrese cedula");
                        cedula = sc.nextLine();  }
                    System.out.println("Ingrese el nombre del Cliente: ");
                    String nombre15= sc.nextLine().toUpperCase();
                    sc.nextLine();
                    System.out.println("Ingrese el email del Cliente: ");
                    String email15= sc.nextLine().toUpperCase();
                    sc.nextLine();
                    System.out.println("Ingrese el telefono del Cliente: ");
                    String telefono15= sc.nextLine().toUpperCase();
                    sc.nextLine();
                    personas.add(new Cliente(cedula,nombre15,telefono15,email15));

                }else{
                    System.out.print("Opcion no valida\n");
                }
            }
            else if (op1 ==5){          //opciones para Cita
                int el  =1;
                while (1<=el){          //menu cita
                    System.out.println("1. Crear Cita\n 2. Eliminar Cita\n 3. Consultar Cita por fecha\n Ingrese la opción que desea:");
                    el = sc.nextInt();
                    int z=0;
                    if ((1<=el) && (el<=3)){
                        el =0;
                        switch(el){
                            case 1:          //crear cita
                                while(z==0){
                                    this.mostrarClientes();
                                    System.out.println("Ingrese el numero de cedula del Cliente: ");
                                    String cedula = sc.nextLine().toUpperCase();
                                    sc.nextLine();
                                    Persona cl = new Cliente(cedula);
                                    if(personas.contains(cl)){
                                        Cliente c1 = this.obtenerCliente(cedula);
                                        this.mostrarEmpleados();
                                        System.out.println("Ingrese el numero de cedula del Empleado: ");
                                        String cedula12 = sc.nextLine().toUpperCase();
                                        sc.nextLine();
                                        Persona dl = new Empleado(cedula12);
                                        if(personas.contains(dl)){
                                            Empleado emp = this.obtenerEmpleado(cedula);
                                            System.out.println("Ingrese la fecha (d/m/año): ");
                                            String f = sc.nextLine().toUpperCase();
                                            sc.nextLine();
                                            System.out.println("Ingrese la hora (H:M): ");
                                            String h = sc.nextLine().toUpperCase();
                                            sc.nextLine();
                                            if (this.validarCita(f,h,c1,emp)){
                                                citas.add(new Cita(f,h,c1,emp));
                                                c1.getCitas().add(citas.get(-1));
                                                z=1;
                                                el=0;
                                            }
                                            
                                        }
                                    }
                                }break;
                            case 2:          //eliminar cita
                                while(z==0){
                                    this.mostrarClientes();
                                    System.out.println("Ingrese el numero de cedula del Cliente: ");
                                    String cedula = sc.nextLine().toUpperCase();
                                    sc.nextLine();
                                    Persona cl = new Cliente(cedula);
                                    if(personas.contains(cl)){
                                           Cliente ct1 = this.obtenerCliente(cedula);
                                           int n = 1;
                                           for (Cita c : ct1.getCitas()){
                                               System.out.print(n+". ");
                                                c.toString();
                                                System.out.print("\n");
                                                n+=1;
                                           }
                                            System.out.println("Ingrese el numero de la Cita a eliminar: ");
                                            int o = sc.nextInt();
                                            sc.nextLine();
                                            citas.remove(ct1.getCitas().get(o-1));
                                            ct1.getCitas().remove(o-1);
                                            z=1;
                                            el=0;
                                    }
                                }break;
                            case 3:          //condultar cita por fecha
                                System.out.println("Ingrese la fecha (d/m/año): ");
                                String fecha = sc.nextLine().toUpperCase();
                                sc.nextLine();
                                this.consultarCita(fecha);
                                el=0;
                        }
                    }else{
                        System.out.print("Opcion no valida\n");
                    }
                }
                 
            }
            else if (op1 ==6){          //opciones para atenciones
                int el  =1;
                while (1<=el){          //menu atencion
                    System.out.println("1. Crear Atencion\n 2. Atencion por Cliente\n 3. Atencion por Empelado\n "
                            + "4. Atencion por fecha\n Ingrese la opción que desea:");
                    el = sc.nextInt();
                    int z=0;
                    if ((1<=el) && (el<=4)){
                        el =0;
                        switch(el){
                            case 1:          //crear atencion
                                while(z==0){
                                   this.mostrarClientes();
                                    System.out.println("Ingrese el numero de cedula del Cliente: ");
                                    String cedula = sc.nextLine().toUpperCase();
                                    sc.nextLine();
                                    Persona cl = new Cliente(cedula);
                                    if(personas.contains(cl)){
                                        Cliente c1 = this.obtenerCliente(cedula);
                                        this.mostrarEmpleados();
                                        System.out.println("Ingrese el numero de cedula del Empleado: ");
                                        String cedula12 = sc.nextLine().toUpperCase();
                                        sc.nextLine();
                                        Persona dl = new Empleado(cedula12);
                                        if(personas.contains(dl)){
                                            Empleado emp = this.obtenerEmpleado(cedula);
                                            System.out.println("Ingrese la duracion : ");
                                            int d = sc.nextInt();
                                            sc.nextLine();
                                            int n = 1;
                                           for (Cita c : c1.getCitas()){
                                               System.out.print(n+". ");
                                                c.toString();
                                                System.out.print("\n");
                                                n+=1;
                                           }
                                            System.out.println("Ingrese el numero de la Cita: ");
                                            int indice = sc.nextInt();
                                            sc.nextLine();
                                            Cita g= c1.getCitas().get(indice-1);
                                            indice = citas.indexOf(g);
                                            this.mostrarServicios();
                                            System.out.println("Ingrese el numero del Servicio: ");
                                            int numero = sc.nextInt();
                                            sc.nextLine();
                                            if((servicios.contains(servicios.get(numero-1)))&&
                                                    (citas.contains(citas.get(indice)))){
                                                atenciones.add(new Atencion(citas.get(indice),servicios.get(numero-1),
                                                emp,c1,d));
                                                atenciones.get(-1).citaAtendida();
                                                z=1;
                                                el=0;
                                            }   
                                        }
                                    }
                                }
                            case 2:          //consutlar atenciones por cliente
                                while(z==0){
                                   this.mostrarClientes();
                                    System.out.println("Ingrese el numero de cedula del Cliente: ");
                                    String cedula = sc.nextLine().toUpperCase();
                                    sc.nextLine();
                                    Persona cl = new Cliente(cedula);
                                    if(personas.contains(cl)){
                                        Cliente c1 = this.obtenerCliente(cedula);
                                        for (Atencion a: atenciones){
                                           if (a.toString().contains(c1.getNombre())){
                                                System.out.println(a.toString());
                                                z=1;
                                                el=0;
                                           }
                                        }
                                    }
                                }
                            case 3:          //consultar atenciones por empleado
                                while(z==0){
                                   this.mostrarEmpleados();
                                    System.out.println("Ingrese el numero de cedula del Empleado: ");
                                    String cedula = sc.nextLine().toUpperCase();
                                    sc.nextLine();
                                    Persona cl = new Empleado(cedula);
                                    if(personas.contains(cl)){
                                        Empleado c1 = this.obtenerEmpleado(cedula);
                                        for (Atencion a: atenciones){
                                           if (a.toString().contains(c1.getNombre())){
                                                System.out.println(a.toString());
                                                z=1;
                                                el=0;
                                           }
                                        }
                                    }
                                }
                            case 4:          //consultar atenciones por fecha
                                while(z==0){
                                    System.out.println("Ingrese la fecha (d/m/año): ");
                                    String fecha = sc.nextLine().toUpperCase();
                                    sc.nextLine();
                                    Cita micita = new Cita(fecha);
                                    for(Cita citaL : citas){
                                        if(micita.equals(citaL)){
                                            for (Atencion a: atenciones){
                                                if(a.getCita().equals(citaL)){
                                                    System.out.println(a.toString());
                                                    z=1;
                                                    el=0;
                                                }
                                            }
                                        }
                                    }     
                            }
                        }
                        }else{
                        System.out.print("Opcion no valida\n");
                        }
                    }
                
            }
            else if(op1==7){          //salir
                System.out.println("Saliendo del programa");
                op+=10;
            }else{
                System.out.println("Opcion no valida");
            }
        }
    }
    public void menuModifica(int v){          //opciones para modificar o editar
        Scanner sh = new Scanner(System.in);
        int eleccion = 1;
        if (v==1){         //modificar negocio
            while ((1<=eleccion) && (eleccion<=7)){
                System.out.println(" 1. Nombre\n 2. Telefono\n 3. Email\n 4. Direccion\n 5. HoraInicio\n 6. HoraFin\n"
                        + " 7. Administrador\n Ingrese el número de la opción que desea modificar: ");
                int dato = sh.nextInt();
                sh.nextLine();
                if ((1<=dato) && (dato<=7)){
                    eleccion=0;
                    switch (dato){
                        case 1:
                            System.out.println("Escriba el nombre y el apellido: "); String n = sh.nextLine().toUpperCase();
                            this.setNombreN(n);
                            break;
                        case 2:
                            System.out.println("Escriba el nuevo telefono: "); String a = sh.nextLine().toUpperCase();
                            this.setTelefonoN(a);
                            break;
                        case 3:
                            System.out.println("Escriba el nuevo email: "); String b = sh.nextLine().toUpperCase();
                            this.setEmailN(b);
                            break;
                        case 4:
                            System.out.println("Escriba la nueva direccion: "); String c = sh.nextLine().toUpperCase();
                            this.setDireccion(c);
                            break;
                        case 5:
                            System.out.println("Escriba la nuevo Hora de Inicio(H:M): "); String d = sh.nextLine().toUpperCase();
                            this.setHoraInicioAtencion(d);
                            break;
                        case 6:
                            System.out.println("Escriba la nuevo Hora de Fin (H:M): "); String e = sh.nextLine().toUpperCase();
                            this.setHoraFinAtencion(e);
                            break;
                        case 7:
                            int z = 0;
                            while (z==0){
                                this.mostrarEmpleados();
                                System.out.println("Ingrese el numero de cedula del nuevo administrador: ");
                                String cedula = sh.nextLine().toUpperCase();
                                Persona cl = new Empleado(cedula);
                                if(personas.contains(cl)){
                                    Empleado emp = this.obtenerEmpleado(cedula);
                                    this.setAdministrador(emp);
                                    z=1;
                                    break;
                                }
                            }
                    }
                sh.nextLine();
                }else{
                    System.out.println("Opcion no valida.\n");
                }
                        
                        
                    
            }
        }else if(v==2){        //modificar servicio
            while ((1<=eleccion) && (eleccion<=servicios.size())){
                this.mostrarServicios();
                System.out.println("Ingrese el número de la del servicio que desea modificar: ");
                int numero = sh.nextInt();
                sh.nextLine();
                if ((1<=numero) && (numero<=servicios.size())){
                    System.out.println("1. Nombre\n 2. Duracion\n 3. Precio\n  Ingrese la opción que desea modificar: ");
                    int valor = sh.nextInt();
                    sh.nextLine();
                    if ((1<=valor) && (valor<=3)){
                        eleccion=0;
                        switch (valor){
                            case 1:
                                System.out.println("Ingrese el nuevo nombre: ");
                                String nombre = sh.nextLine().toUpperCase();
                                servicios.get(numero-1).setNombreS(nombre);
                                break;
                            case 2:
                                System.out.println("Ingrese la nueva duración en minutos: ");
                                int dura = sh.nextInt();
                                servicios.get(numero-1).setDuracion(dura);
                                break;
                            case 3:
                                System.out.println("Ingrese el nuevo precio");
                                double precio = sh.nextDouble();
                                servicios.get(numero-1).setPrecio(precio);
                                break;
                        }
                    sh.nextLine();
                    }else{
                        System.out.println("Opcion no valida.\n");
                    }
                }else{
                    System.out.println("Opcion no valida.\n");  
                } 
            }
        }else if(v==3){        //modificar empleado
            while (1<=eleccion){
                this.mostrarEmpleados();
                System.out.println("Ingrese el número de la del cedula del empleado que desea modificar: ");
                String cedula = sh.nextLine().toUpperCase();
                sh.nextLine();
                Persona empl = new Empleado(cedula);
                if(personas.contains(empl)){
                    Empleado empl_edit = this.obtenerEmpleado(cedula);
                    System.out.println("1. Cedula\n 2. Nombre\n 3. Telefono\n 4. Email\n  Ingrese la opción que desea modificar: ");
                    int opcion = sh.nextInt();
                    sh.nextLine();
                    if ((1<=opcion) && (opcion<=4)){
                        eleccion=0;
                        switch (opcion){
                            case 1:
                                System.out.println("Ingrese la nueva cedula: ");
                                String cedula01 = sh.nextLine().toUpperCase();
                                empl_edit.setCedula(cedula01);
                                break;
                            case 2:
                                System.out.println("Ingrese la nuevo nombre: ");
                                String nombre = sh.nextLine().toUpperCase();
                                empl_edit.setNombre(nombre);
                                break;
                            case 3:
                                System.out.println("Ingrese el nuevo telefono");
                                String telef = sh.nextLine().toUpperCase();
                                empl_edit.setTelefono(telef);
                                break;
                            case 4:
                                System.out.println("Ingrese el nuevo email");
                                String email = sh.nextLine().toUpperCase();
                                empl_edit.setEmail(email);
                                break;
                        }
                    sh.nextLine();
                    }else{
                        System.out.println("Opcion no valida.\n");
                    }
                }else{
                    System.out.println("Cédula no encontrada.\n");
                }
            }
        }else if(v==4){        //modificar cliente
            while (1<=eleccion){
                this.mostrarClientes();
                System.out.println("Ingrese el número de la del cedula del cliente que desea modificar: ");
                String cedula = sh.nextLine().toUpperCase();
                sh.nextLine();
                Persona cl = new Cliente(cedula);
                if(personas.contains(cl)){
                    Cliente client_edit = this.obtenerCliente(cedula);
                    System.out.println("1.Email\n 2. Nombre\n 3. Telefono\n    Ingrese la opción que desea modificar: ");
                    int opcion = sh.nextInt();
                    sh.nextLine();
                    if ((1<=opcion) && (opcion<=3)){
                        eleccion=0;
                        switch (opcion){
                            case 1:
                               System.out.println("Ingrese el nuevo email");
                                String email = sh.nextLine().toUpperCase();
                                client_edit.setEmail(email);
                                break;
                            case 2:
                                System.out.println("Ingrese el nuevo nombre: ");
                                String nombre = sh.nextLine().toUpperCase();
                                client_edit.setNombre(nombre);
                                break;
                            case 3:
                                System.out.println("Ingrese el nuevo telefono");
                                String telef = sh.nextLine().toUpperCase();
                                client_edit.setTelefono(telef);
                                break;
                        }
                    sh.nextLine();
                    }else{
                        System.out.println("Opcion no valida.\n");
                    }
                }else{
                    System.out.println("Cédula no encontrada.\n");
                }
            }
        }
        
    }
        //meotodos para ver clientes, empleados, antenciones y servicios
    public void mostrarClientes(){
        for(Persona cadcliente: personas){
            if(cadcliente instanceof Cliente){
                System.out.println(cadcliente.toString());
            }
        }
    
    }  
    public void mostrarEmpleados(){
        for(Persona cadempleado : personas){
            if(cadempleado instanceof Empleado){
            System.out.println(cadempleado.toString());
            }
        }
    }

    public void mostrarAtenciones(){
        int i = 1;
        for(Atencion a: atenciones){
            System.out.print(i+". ");
            System.out.print(a.toString());
            System.out.print("\n");
            i+=1;
        }
    }
    public void mostrarServicios(){
        int i = 1;
        for(Servicio s: servicios){
            System.out.print(i+". ");
            System.out.print(s.toString());
            System.out.print("\n");
            i+=1;
        }
    }
            //metodos para obtner empleados y clientes
    public Empleado obtenerEmpleado(String ced){
        Persona empl = new Empleado(ced);
        if(personas.contains(empl)){
            int indice = personas.indexOf(empl);
            return (Empleado)personas.get(indice);
        }else{
            return null;
        }
       
    }
    public Cliente obtenerCliente(String ced){
        Persona cl = new Cliente(ced);
        if(personas.contains(cl)){
            int indice = personas.indexOf(cl);
            return (Cliente)personas.get(indice);
        }else{
            return null;
        }
       
    }
    //verificar si persona existe  por la cedula en la lista
    public boolean personaExiste(String cedulaID){
        Persona busquedaC = new Cliente(cedulaID);
        return personas.contains(busquedaC);
    }
    
    //validar cita
   public boolean validarCita(String fecha, String Hora, Cliente c, Empleado em ){
       Cita ct = new Cita(fecha,Hora,c,em);
       for (Cita b:citas){
            if ((b.getFecha().equals(ct.getFecha()))&&(b.getEmpleado().equals(ct.getEmpleado()))&&(b.getHora().equals(ct.getHora()))){
               return false;
            }else{
               System.out.print("Cita imposible de crear.");
               return true;
            }
       }       
        return false;
    }
}
 